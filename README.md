# Backend (backend/)
1. Instalar los paquetes de node con:
    * yarn o npm install

2. Revisar las variables de entorno del archivo de configuración que se encuentra en:
    * server/config/env.json
Con la finalidad de que se pueda establecer correctamente la conexión con la base de datos, correr la aplicación en el puerto deseado y agregar una palabra secreta para cifrar los tokens.
*NOTA: Al cambiar el puerto se requeriría cambiar las urls del frontend.*

3. Correr la aplicación con:
    * yarn start o npm start

4. Hacer una petición POST a ‘/usuarios’ con el siguiente formato para crear un usuario con rol de administrador:
{
“nombre”:  “David Giovanny”,
“user”:  “dgio”
“pass”:  “pass123”
“role”:  “Admin”
}

5. Hacer una petición POST a ‘/usuarios’ con el siguiente formato para crear un usuario con rol de cliente:
{
“nombre”:  “Alexia Ariana”,
“user”:  “alezana”
“pass”:  “pass123”
}

# Frontend (frontend/)
1. Tener corriendo el servidor en el puerto establecido.

2. Instalar los paquetes de node con:
    * yarn o npm install

3. Correr la aplicación con:
    * yarn start o npm start
*NOTA: Por defecto la aplicación corre el en puerto 8000, así que, para acceder a la aplicación, se tiene que entrar htttp://localhost:8000/*


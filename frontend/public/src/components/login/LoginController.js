angular.module('app')
    .controller('LoginController', function (LoginResource, $window, $location) {
        const self = this;
        self.credenciales = {};

        self.onlogin = function () {
            LoginResource.save(self.credenciales).$promise.then(function (res) {
                const data = JSON.parse(res.data);
                let usuario = {
                    nombre: data.nombre,
                    user: data.user,
                    role: data.role,
                    token: res.headers.authorization
                }
                $window.sessionStorage.setItem('usuario', JSON.stringify(usuario));
                $location.path('/');
            });
        }
    });
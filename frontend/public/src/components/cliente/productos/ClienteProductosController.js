angular.module('app')
    .controller('ClienteProductosController', function ($scope, ProductoResource, $location, $window) {

        $scope.items = [];
        $scope.carrito = [];

        $scope.getAll = function () {
            const carrito = JSON.parse($window.localStorage.getItem('carrito'));
            $scope.carrito = carrito ? carrito : [];
            $scope.items = ProductoResource.query();
        }

        $scope.agregarProducto = function (item) {
            let add = true;
            $scope.carrito.forEach(i => {
                if (i._id === item._id) add = false;
            });
            item.mincant = 1;
            if (add) $scope.carrito.push(item);

            $window.localStorage.setItem('carrito', JSON.stringify($scope.carrito));

            $location.path('/cliente/carrito');
        }
    })
    .controller('ClienteCarritoController', function ($scope, VentaResource, $location, $window) {
        $scope.items = [];
        $scope.total = 0;
        $scope.mensaje = '';

        $scope.getAll = function () {
            const items = JSON.parse($window.localStorage.getItem('carrito'));
            $scope.items = items ? items : [];
        }

        $scope.pagar = function () {
            let carrito = [];
            $scope.items.forEach(i => {
                carrito.push({ _id: i._id, cantidad: i.mincant });
            });
            VentaResource.save(carrito).$promise.then(function (res) {
                $window.localStorage.removeItem('carrito');
                $location.path('/');
            }, function (error) {
                $scope.mensaje = error.data && error.data.mensaje ? error.data.mensaje : 'Hubo un error al procesar el pago';
            });
        }

        $scope.actualizarCarrito = function () {
            $scope.total = 0;
            $scope.items.forEach(i => {
                $scope.total += i.precio * i.mincant
            });

            $window.localStorage.setItem('carrito', JSON.stringify($scope.items));
        }

        $scope.quitarProducto = function (item) {
            $scope.items.splice($scope.items.indexOf(item), 1);
            $scope.actualizarCarrito();
        }
    })
    .controller('RecargaController', function ($scope, RecargaResource, $location) {
        $scope.cartera = {};

        $scope.recargar = function () {
            RecargaResource.update($scope.cartera).$promise.then(function (res) {
                $location.path('/');
            }, function (error) {
                $scope.mensaje = error.data && error.data.mensaje ? error.data.mensaje : 'Hubo un error al procesar el pago';
            });
        }
    });
angular.module('app')
    .controller('ProductosController', function ($scope, $timeout, ProductoResource, $location) {

        $scope.selected = [];

        $scope.query = {
            order: 'nombre',
            limit: 5,
            page: 1
        };

        $scope.items = [];

        self.getAll = function () {
            $scope.items = ProductoResource.query();
        }

        $scope.nuevoProducto = function (i) {
            $location.path('/admin/productos/nuevo');
        }

        $scope.editarProducto = function (i) {
            $location.path('/admin/productos/editar/' + $scope.selected[0]._id);
        }

        $scope.detalleProducto = function () {
            $location.path('/admin/productos/detalle/' + $scope.selected[0]._id);
        }

        $scope.loadStuff = function () {
            $scope.promise = $timeout(function () {
                self.getAll()
            }, 500);
        };
    })
    .controller('ProductoDetalleController', function ($location, $scope, ProductoResource, $routeParams) {
        $scope.title = 'Detalle de producto';
        $scope.producto = ProductoResource.get({ id: $routeParams.id });

        $scope.regresar = function () {
            $location.path('/admin/productos');
        };
    })
    .controller('ProductoFormularioController', function ($location, $scope, ProductoResource, $routeParams) {
        self = this;
        let id = $routeParams.id;
        $scope.producto = {};

        if (id) {
            $scope.title = 'Editar producto';
            $scope.producto = ProductoResource.get({ id });
            $scope.submitForm = function () {
                ProductoResource.update({ id }, $scope.producto).$promise.then(function (res) {
                    self.productos();
                });
            }
        } else {
            $scope.title = 'Nuevo producto';
            $scope.submitForm = function () {
                ProductoResource.save($scope.producto).$promise.then(function (res) {
                    self.productos();
                })
            };
        }

        self.productos = function () {
            $location.path('/admin/productos');
        };

        $scope.regresar = self.productos;
    });
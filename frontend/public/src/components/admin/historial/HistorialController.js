angular.module('app')
    .controller('HistorialController', function ($scope, $timeout, HistorialResource, $location) {

        $scope.selected = [];

        $scope.query = {
            order: 'nombre',
            limit: 5,
            page: 1
        };

        $scope.items = [];

        self.getAll = function () {
            $scope.items = HistorialResource.query();
        }

        $scope.detalleVenta = function () {
            $location.path('/admin/historial/detalle/' + $scope.selected[0]._id);
        }

        $scope.loadStuff = function () {
            $scope.promise = $timeout(function () {
                self.getAll()
            }, 500);
        };
    })
    .controller('HistorialDetalleController', function ($location, $scope, HistorialResource, $routeParams) {
        $scope.title = 'Detalle de venta';
        $scope.venta = HistorialResource.get({ id: $routeParams.id });

        $scope.regresar = function () {
            $location.path('/admin/historial');
        };
    })
    .controller('ComprasController', function ($scope, $timeout, ComprasResource, $location) {

        $scope.selected = [];

        $scope.query = {
            order: 'creado',
            limit: 5,
            page: 1
        };

        $scope.items = [];

        self.getAll = function () {
            $scope.items = ComprasResource.query();
        }

        $scope.detalleVenta = function () {
            $location.path('/cliente/compras/detalle/' + $scope.selected[0]._id);
        }

        $scope.loadStuff = function () {
            $scope.promise = $timeout(function () {
                self.getAll()
            }, 500);
        };
    })
    .controller('ComprasDetalleController', function ($location, $scope, HistorialResource, $routeParams) {
        $scope.title = 'Detalle de compra';
        $scope.venta = HistorialResource.get({ id: $routeParams.id });

        $scope.regresar = function () {
            $location.path('/cliente/compras');
        };
    });
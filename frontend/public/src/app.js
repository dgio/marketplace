angular.module('app', ['ngMaterial', 'md.data.table', 'ngMessages', 'ngRoute', 'ngResource'])
    .controller('AppController', function ($mdSidenav, $scope, $window, $location, LogoutResource) {
        const self = this;
        $scope.usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        self.toggleList = function () {
            $mdSidenav('left').toggle();
        }

        self.logout = function () {
            LogoutResource.remove().$promise.then(function (res) {
                $window.sessionStorage.removeItem('usuario');
                $location.path('/login');
            });
        }
    })
    .run(function ($rootScope, $location) {
        $rootScope.$on('$routeChangeError', function () {
            $location.path('/login');
        })
    });
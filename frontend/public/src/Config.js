angular.module('app')
    .config(function ($mdThemingProvider, $routeProvider, $locationProvider) {
        $mdThemingProvider.theme('default')
            .primaryPalette('orange')
            .accentPalette('orange')
            .warnPalette('orange');

        $locationProvider.html5Mode(true);

        const auth = {
            'auth': function (AuthService) {
                return AuthService.authenticate();
            }
        }

        $routeProvider
            .when('/', {
                templateUrl: 'src/components/inicio.html',
                resolve: auth
            })
            .when('/login', {
                controller: 'LoginController as login',
                templateUrl: 'src/components/login/login.html'
            })
            .when('/registro', {
                //controller: 'RegistroController',
                templateUrl: 'src/components/registro.html'
            })
            .when('/admin/productos', {
                controller: 'ProductosController as productos',
                templateUrl: 'src/components/admin/productos/productos.html',
                resolve: auth
            })
            .when('/admin/productos/detalle/:id', {
                controller: 'ProductoDetalleController as productoDetalle',
                templateUrl: 'src/components/admin/productos/detalle/detalle.html',
                resolve: auth
            })
            .when('/admin/productos/nuevo/', {
                controller: 'ProductoFormularioController as productoFormulario',
                templateUrl: 'src/components/admin/productos/detalle/formulario.html',
                resolve: auth
            })
            .when('/admin/productos/editar/:id?', {
                controller: 'ProductoFormularioController as productoFormulario',
                templateUrl: 'src/components/admin/productos/detalle/formulario.html',
                resolve: auth
            })
            .when('/admin/historial', {
                controller: 'HistorialController as historial',
                templateUrl: 'src/components/admin/historial/historial.html',
                resolve: auth
            })
            .when('/admin/historial/detalle/:id', {
                controller: 'HistorialDetalleController as historial',
                templateUrl: 'src/components/admin/historial/detalle/detalle.html',
                resolve: auth
            })
            .when('/cliente/productos', {
                controller: 'ClienteProductosController as clienteProductos',
                templateUrl: 'src/components/cliente/productos/productos.html',
                resolve: auth
            })
            .when('/cliente/carrito', {
                controller: 'ClienteCarritoController as clienteCarrito',
                templateUrl: 'src/components/cliente/productos/carrito.html',
                resolve: auth
            })
            .when('/cliente/compras', {
                controller: 'ComprasController as compras',
                templateUrl: 'src/components/cliente/compras/compras.html',
                resolve: auth
            })
            .when('/cliente/compras/detalle/:id', {
                controller: 'ComprasDetalleController as compras',
                templateUrl: 'src/components/cliente/compras/detalle/detalle.html',
                resolve: auth
            })
            .when('/cliente/recargar', {
                controller: 'RecargaController as recarga',
                templateUrl: 'src/components/cliente/recargar.html',
                resolve: auth
            });
    });
angular.module('app')
    .factory('ProductoResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        return $resource('http://localhost:3003/productos/:id', { id: '@id' },
            {
                save: {
                    method: 'POST',
                    headers: { 'Authorization': usuario.token }
                },
                query: {
                    method: 'GET',
                    isArray: true,
                    headers: { 'Authorization': usuario.token }
                },
                get: {
                    method: 'GET',
                    headers: { 'Authorization': usuario.token }
                },
                update: {
                    method: 'PUT',
                    headers: { 'Authorization': usuario.token }
                },
                remove: {
                    method: 'DELETE',
                    headers: { 'Authorization': usuario.token }
                }
            });
    })
    .factory('VentaResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        const token = usuario ? usuario.token : ''
        return $resource('http://localhost:3003/ventas', {},
            {
                save: {
                    method: 'POST',
                    headers: { 'Authorization': token }
                }
            });
    })
    .factory('RecargaResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        const token = usuario ? usuario.token : ''
        return $resource('http://localhost:3003/recarga', {},
            {
                update: {
                    method: 'PUT',
                    headers: { 'Authorization': token }
                }
            });
    });
angular.module('app')
    .factory('AuthService', function ($q, $window) {
        return {
            authenticate: function () {
                const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
                return usuario && usuario.token ? true : $q.reject();
            }
        }
    })
    .factory('LoginResource', function ($resource) {
        return $resource('http://localhost:3003/login', {},
            {
                save: {
                    method: 'POST',
                    transformResponse: function (data, headers) {
                        response = {}
                        response.data = data;
                        response.headers = headers();
                        return response;
                    }
                }
            }
        );
    })
    .factory('LogoutResource', function ($resource, $window) {
        const usuario = JSON.parse($window.sessionStorage.getItem('usuario'));
        const token = usuario ? usuario.token : ''
        return $resource('http://localhost:3003/logout', {},
            {
                remove: {
                    method: 'DELETE',
                    headers: { 'Authorization': token }
                }
            }
        );
    });




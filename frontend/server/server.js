const express = require('express');

const app = express();
const staticApp = express.static('public');

const port = process.env.PORT || 8000;

app.use('/', staticApp);
app.use('*', staticApp);

app.listen(port, () => {
    console.log(`Servidor iniciado en el puerto ${port}`);
});

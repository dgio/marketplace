var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI).catch((err) => {
    console.error(err.stack);
    process.exit(1);
});

module.exports = { mongoose };

const { Usuario } = require('../models/usuario');

const authenticate = (req, res, next) => {
  const token = req.header('Authorization');

  Usuario.findByToken(token).then((usuario) => {
    if (!usuario) return Promise.reject();

    req.usuario = usuario;
    req.token = token;
    next();
  }).catch((e) => {
    res.status(401).send();
  });
};

module.exports = { authenticate };

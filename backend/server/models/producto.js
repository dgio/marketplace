const mongoose = require('mongoose');

const ProductoSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true,
        minlength: 3
    },
    descripcion: {
        type: String,
        trim: true
    },
    precio: {
        type: Number,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    img: {
        type: String,
        trim: true
    },
    creado: {
        type: Date,
        default: Date.now
    }
});

const Producto = mongoose.model('Producto', ProductoSchema);

module.exports = { Producto };
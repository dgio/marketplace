const mongoose = require('mongoose');

const HistorialSchema = new mongoose.Schema({
    _usuario: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    user: {
        type: String,
        required: true,
        trim: true,
        minlength: 3
    },
    nombre: {
        type: String,
        required: true,
        trim: true
    },
    total: {
        type: Number,
        required: true,
        trim: true
    },
    detalle: [
        {
            _id: {
                type: mongoose.Schema.Types.ObjectId,
                required: true
            },
            nombre: {
                type: String,
                required: true,
                trim: true,
                minlength: 3
            },
            descripcion: {
                type: String,
                trim: true
            },
            precio: {
                type: Number,
                required: true
            },
            cantidad: {
                type: Number,
                required: true
            },
            img: {
                type: String,
                trim: true
            }
        }
    ],
    creado: {
        type: Date,
        default: Date.now
    }
});

const Historial = mongoose.model('Historial', HistorialSchema);

module.exports = { Historial };